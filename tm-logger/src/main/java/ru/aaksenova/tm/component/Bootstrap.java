package ru.aaksenova.tm.component;

import lombok.SneakyThrows;
import org.apache.activemq.ActiveMQConnection;
import org.apache.activemq.ActiveMQConnectionFactory;
import org.jetbrains.annotations.NotNull;
import ru.aaksenova.tm.listener.EntityListener;
import ru.aaksenova.tm.service.LoggerService;

import javax.jms.*;


public final class Bootstrap {

    @NotNull
    private static final String URL = ActiveMQConnection.DEFAULT_BROKER_URL;

    @NotNull
    private static final String QUEUE = "LOGGER";

    @SneakyThrows
    public void start() {
        final LoggerService loggerService = new LoggerService();
        final EntityListener entityListener = new EntityListener(loggerService);
        final ConnectionFactory connectionFactory = new ActiveMQConnectionFactory(URL);
        final Connection connection = connectionFactory.createConnection();
        connection.start();
        final Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
        final Queue destination = session.createQueue(QUEUE);
        final MessageConsumer messageConsumer = session.createConsumer(destination);
        messageConsumer.setMessageListener(entityListener);
    }


}
