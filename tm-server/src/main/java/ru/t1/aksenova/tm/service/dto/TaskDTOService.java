package ru.t1.aksenova.tm.service.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.aksenova.tm.api.repository.dto.ITaskDTORepository;
import ru.t1.aksenova.tm.api.service.IConnectionService;
import ru.t1.aksenova.tm.api.service.dto.ITaskDTOService;
import ru.t1.aksenova.tm.dto.model.TaskDTO;
import ru.t1.aksenova.tm.enumerated.Status;
import ru.t1.aksenova.tm.enumerated.TaskSort;
import ru.t1.aksenova.tm.exception.entity.TaskNotFoundException;
import ru.t1.aksenova.tm.exception.field.*;
import ru.t1.aksenova.tm.repository.dto.TaskDTORepository;

import javax.persistence.EntityManager;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class TaskDTOService extends AbstractUserOwnedDTOService<TaskDTO, ITaskDTORepository>
        implements ITaskDTOService {

    public TaskDTOService(@NotNull final IConnectionService connectionService) {
        super(connectionService);
    }

    @NotNull
    @Override
    public ITaskDTORepository getRepository(@NotNull final EntityManager entityManager) {
        return new TaskDTORepository(entityManager);
    }

    @NotNull
    @Override
    public TaskDTO create(@Nullable final TaskDTO task) {
        if (task == null) throw new TaskNotFoundException();
        add(task);
        return task;
    }

    @NotNull
    @Override
    public TaskDTO create(
            @Nullable final String userId,
            @Nullable final TaskDTO task
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (task == null) throw new TaskNotFoundException();
        task.setUserId(userId);
        add(task);
        return task;
    }

    @NotNull
    @Override
    public TaskDTO create(
            @Nullable final String userId,
            @Nullable final String name,
            @Nullable final String description
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (description == null || description.isEmpty()) throw new DescriptionEmptyException();
        @NotNull TaskDTO task = new TaskDTO();
        task.setUserId(userId);
        task.setName(name);
        task.setDescription(description);
        add(task);
        return task;
    }

    @NotNull
    @Override
    public Collection<TaskDTO> set(@NotNull Collection<TaskDTO> tasks) {
        if (tasks.isEmpty()) return Collections.emptyList();
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final ITaskDTORepository repository = getRepository(entityManager);
            entityManager.getTransaction().begin();
            tasks.forEach(repository::add);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
        return tasks;
    }


    @NotNull
    @Override
    public List<TaskDTO> findAll() {
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final ITaskDTORepository repository = getRepository(entityManager);
            return repository.findAll();
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    @Override
    public List<TaskDTO> findAll(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final ITaskDTORepository repository = getRepository(entityManager);
            return repository.findAll(userId);
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    @Override
    public List<TaskDTO> findAll(@Nullable final String userId, @Nullable final TaskSort sort) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (sort == null) return findAll(userId);
        return findAll(userId, sort.getComparator());
    }

    @NotNull
    @Override
    public List<TaskDTO> findAll(
            @Nullable final String userId,
            @Nullable final Comparator<TaskDTO> comparator
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (comparator == null) return findAll(userId);
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final ITaskDTORepository repository = getRepository(entityManager);
            @NotNull final List<TaskDTO> tasks = repository.findAll(userId, comparator);
            if (tasks.isEmpty() || tasks == null) return Collections.emptyList();
            return tasks;
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    @Override
    public List<TaskDTO> findAllByProjectId(
            @Nullable final String userId,
            @Nullable final String projectId
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (projectId == null || projectId.isEmpty()) throw new ProjectIdEmptyException();
        @NotNull List<TaskDTO> tasks;
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final ITaskDTORepository repository = getRepository(entityManager);
            tasks = repository.findAllByProjectId(userId, projectId);
        } finally {
            entityManager.close();
        }
        return tasks;
    }

    @Nullable
    @Override
    public TaskDTO findOneById(
            @Nullable final String userId,
            @Nullable final String id
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final ITaskDTORepository repository = getRepository(entityManager);
            @Nullable final TaskDTO task = repository.findOneById(userId, id);
            if (task == null) throw new TaskNotFoundException();
            return task;
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void clear() {
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final ITaskDTORepository repository = getRepository(entityManager);
            entityManager.getTransaction().begin();
            repository.clear();
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void removeAll(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final ITaskDTORepository repository = getRepository(entityManager);
            entityManager.getTransaction().begin();
            repository.clear(userId);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Nullable
    @Override
    public TaskDTO removeOne(
            @Nullable final String userId,
            @Nullable final TaskDTO task
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (task == null) throw new TaskNotFoundException();
        remove(userId, task);
        return task;
    }

    @Nullable
    @Override
    public TaskDTO removeOneById(
            @Nullable final String userId,
            @Nullable final String id
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @Nullable final TaskDTO task = findOneById(userId, id);
        remove(userId, task);
        return task;
    }

    @NotNull
    @Override
    public TaskDTO updateById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final String name,
            @Nullable final String description
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (description == null || description.isEmpty()) throw new DescriptionEmptyException();
        @Nullable final TaskDTO task = findOneById(userId, id);
        if (task == null) throw new TaskNotFoundException();
        task.setName(name);
        task.setDescription(description);
        update(task);
        return task;
    }

    @NotNull
    @Override
    public TaskDTO changeTaskStatusById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final Status status
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (status == null) throw new StatusIncorrectException();
        @Nullable final TaskDTO task = findOneById(userId, id);
        if (task == null) throw new TaskNotFoundException();
        task.setStatus(status);
        update(task);
        return task;
    }

    @Override
    public boolean existsById(@Nullable final String userId, @Nullable final String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final ITaskDTORepository repository = getRepository(entityManager);
            return (repository.findOneById(userId, id) != null);
        } finally {
            entityManager.close();
        }
    }

    @Override
    public long getSize(@Nullable final String userId) {
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final ITaskDTORepository repository = getRepository(entityManager);
            return repository.getCount(userId);
        } finally {
            entityManager.close();
        }
    }

}
