package ru.t1.aksenova.tm.repository.dto;

import org.hibernate.jpa.QueryHints;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.aksenova.tm.api.repository.dto.IUserDTORepository;
import ru.t1.aksenova.tm.dto.model.UserDTO;

import javax.persistence.EntityManager;
import java.util.List;
import java.util.Optional;

public class UserDTORepository extends AbstractDTORepository<UserDTO> implements IUserDTORepository {

    public UserDTORepository(@NotNull final EntityManager entityManager) {
        super(entityManager);
    }

    @NotNull
    @Override
    public List<UserDTO> findAll() {
        @NotNull final String jpql = "SELECT m FROM UserDTO m";
        return entityManager.createQuery(jpql, UserDTO.class)
                .setHint(QueryHints.HINT_CACHEABLE, true)
                .getResultList();
    }

    @Nullable
    @Override
    public UserDTO findOneById(@Nullable final String id) {
        if (id.isEmpty() || id == null) return null;
        return entityManager.find(UserDTO.class, id);
    }

    @Nullable
    @Override
    public UserDTO findByLogin(@Nullable final String login) {
        if (login == null || login.isEmpty()) return null;
        @NotNull final String sql = "SELECT m FROM UserDTO m WHERE m.login = :login";
        return entityManager.createQuery(sql, UserDTO.class)
                .setParameter("login", login)
                .setHint(QueryHints.HINT_CACHEABLE, true)
                .setMaxResults(1).getResultList().stream().findFirst().orElse(null);
    }

    @Nullable
    @Override
    public UserDTO findByEmail(@Nullable final String email) {
        if (email == null || email.isEmpty()) return null;
        @NotNull final String sql = "SELECT m FROM UserDTO m WHERE m.email = :email";
        return entityManager.createQuery(sql, UserDTO.class)
                .setParameter("email", email)
                .setHint(QueryHints.HINT_CACHEABLE, true)
                .setMaxResults(1).getResultList().stream().findFirst().orElse(null);
    }

    @Override
    public void removeOneById(@Nullable final String id) {
        if (id == null || id.isEmpty()) return;
        Optional<UserDTO> model = Optional.ofNullable(findOneById(id));
        model.ifPresent(this::remove);
    }

    @Override
    public void clear() {
        @NotNull final String jpql = "DELETE FROM UserDTO";
        entityManager.createQuery(jpql).executeUpdate();
    }

    @Override
    public void removeOneByLogin(@Nullable final String login) {
        Optional<UserDTO> model = Optional.ofNullable(findByLogin(login));
        model.ifPresent(this::remove);
    }

    @Override
    public void removeOneByEmail(@Nullable final String email) {
        Optional<UserDTO> model = Optional.ofNullable(findByEmail(email));
        model.ifPresent(this::remove);
    }

    @Override
    public boolean isLoginExist(@Nullable final String login) {
        if (login == null || login.isEmpty()) return false;
        return findByLogin(login) != null;
    }

    @Override
    public boolean isEmailExist(@Nullable final String email) {
        if (email == null || email.isEmpty()) return false;
        return findByEmail(email) != null;
    }

}
