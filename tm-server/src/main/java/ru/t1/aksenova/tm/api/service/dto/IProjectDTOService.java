package ru.t1.aksenova.tm.api.service.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.aksenova.tm.dto.model.ProjectDTO;
import ru.t1.aksenova.tm.enumerated.ProjectSort;
import ru.t1.aksenova.tm.enumerated.Status;

import java.util.Collection;
import java.util.Comparator;
import java.util.List;

public interface IProjectDTOService extends IUserOwnedDTOService<ProjectDTO> {
    @NotNull
    ProjectDTO create(@Nullable ProjectDTO project);

    @NotNull
    ProjectDTO create(
            @Nullable String userId,
            @Nullable ProjectDTO project
    );

    @NotNull
    ProjectDTO create(
            @Nullable String userId,
            @Nullable String name,
            @Nullable String description
    );

    @NotNull Collection<ProjectDTO> set(@NotNull Collection<ProjectDTO> projects);

    @NotNull
    List<ProjectDTO> findAll();

    @NotNull
    List<ProjectDTO> findAll(@Nullable String userId);

    @NotNull
    List<ProjectDTO> findAll(@Nullable String userId, @Nullable ProjectSort sort);

    @NotNull
    List<ProjectDTO> findAll(
            @Nullable String userId,
            @Nullable Comparator comparator
    );

    @Nullable
    ProjectDTO findOneById(
            @Nullable String userId,
            @Nullable String id
    );

    void clear();

    void removeAll(@Nullable String userId);

    @Nullable
    ProjectDTO removeOne(
            @Nullable String userId,
            @Nullable ProjectDTO project
    );

    @Nullable
    ProjectDTO removeOneById(
            @Nullable String userId,
            @Nullable String id
    );

    @NotNull
    ProjectDTO updateById(
            @Nullable String userId,
            @Nullable String id,
            @Nullable String name,
            @Nullable String description
    );

    @NotNull
    ProjectDTO changeProjectStatusById(
            @Nullable String userId,
            @Nullable String id,
            @Nullable Status status
    );

    boolean existsById(@Nullable String userId, @Nullable String id);

    long getSize(@Nullable String userId);
}
