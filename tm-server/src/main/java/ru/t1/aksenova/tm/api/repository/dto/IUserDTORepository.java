package ru.t1.aksenova.tm.api.repository.dto;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.aksenova.tm.dto.model.UserDTO;

import java.util.List;

public interface IUserDTORepository extends IDTORepository<UserDTO> {

    @NotNull
    List<UserDTO> findAll();

    @Nullable
    UserDTO findOneById(@Nullable String id);

    @Nullable
    UserDTO findByLogin(@Nullable String login);

    @Nullable
    UserDTO findByEmail(@Nullable String email);

    void removeOneById(@Nullable String id);

    void clear();

    void removeOneByLogin(@Nullable String login);

    void removeOneByEmail(@Nullable String email);

    @SneakyThrows
    boolean isLoginExist(@Nullable String login);

    @SneakyThrows
    boolean isEmailExist(@Nullable String email);

}
