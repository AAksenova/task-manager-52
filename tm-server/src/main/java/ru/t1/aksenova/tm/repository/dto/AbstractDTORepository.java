package ru.t1.aksenova.tm.repository.dto;

import org.jetbrains.annotations.NotNull;
import ru.t1.aksenova.tm.api.repository.dto.IDTORepository;
import ru.t1.aksenova.tm.dto.model.AbstractModelDTO;

import javax.persistence.EntityManager;

public abstract class AbstractDTORepository<M extends AbstractModelDTO> implements IDTORepository<M> {

    @NotNull
    protected final EntityManager entityManager;

    public AbstractDTORepository(@NotNull final EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    @Override
    public M add(@NotNull final M model) {
        entityManager.persist(model);
        return model;
    }

    @Override
    public void update(@NotNull final M model) {
        entityManager.merge(model);
    }

    @Override
    public void remove(@NotNull final M model) {
        entityManager.remove(model);
    }

}
