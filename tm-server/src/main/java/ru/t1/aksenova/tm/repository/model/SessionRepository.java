package ru.t1.aksenova.tm.repository.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.aksenova.tm.api.repository.model.ISessionRepository;
import ru.t1.aksenova.tm.model.Session;

import javax.persistence.EntityManager;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

public class SessionRepository extends AbstractUserOwnedRepository<Session> implements ISessionRepository {

    public SessionRepository(@NotNull EntityManager entityManager) {
        super(entityManager);
    }

    @NotNull
    @Override
    public List<Session> findAll() {
        @NotNull final String jpql = "SELECT m FROM Session m";
        return entityManager.createQuery(jpql, Session.class).getResultList();
    }

    @NotNull
    @Override
    public List<Session> findAll(@NotNull final String userId) {
        if (userId.isEmpty() || userId == null) return Collections.emptyList();
        @NotNull final String jpql = "SELECT m FROM Session m WHERE m.user.id = :userId";
        return entityManager.createQuery(jpql, Session.class)
                .setParameter("userId", userId)
                .getResultList();
    }

    @Nullable
    @Override
    public Session findOneById(@NotNull final String id) {
        if (id.isEmpty() || id == null) return null;
        return entityManager.find(Session.class, id);
    }

    @Nullable
    @Override
    public Session findOneById(@NotNull final String userId, @NotNull final String id) {
        if (id.isEmpty() || id == null) return null;
        if (userId.isEmpty() || userId == null) return null;
        @NotNull final String jpql = "SELECT m FROM Session m WHERE m.user.id  = :userId and m.id = :id";
        return entityManager.createQuery(jpql, Session.class)
                .setParameter("userId", userId)
                .setParameter("id", id)
                .setMaxResults(1).getResultList().stream().findFirst().orElse(null);
    }

    @Override
    public void removeById(@NotNull final String id) {
        Optional<Session> model = Optional.ofNullable(findOneById(id));
        model.ifPresent(this::remove);
    }

    @Override
    public void removeById(@NotNull final String userId, @NotNull final String id) {
        Optional<Session> model = Optional.ofNullable(findOneById(id, userId));
        model.ifPresent(this::remove);
    }

    @Override
    public void clear() {
        @NotNull final String jpql = "DELETE FROM Session";
        entityManager.createQuery(jpql).executeUpdate();
    }

    @Override
    public void clear(@NotNull final String userId) {
        @NotNull final String jpql = "DELETE FROM Session m WHERE m.user.id  = :userId";
        entityManager.createQuery(jpql)
                .setParameter("userId", userId)
                .executeUpdate();
    }

    @Override
    public long getCount() {
        @NotNull final String jpql = "SELECT COUNT(m) FROM Session m";
        return entityManager.createQuery(jpql, Long.class).getSingleResult();
    }

    @Override
    public long getCount(@NotNull final String userId) {
        @NotNull final String jpql = "SELECT COUNT(m) FROM Session m WHERE m.user.id  = :userId";
        return entityManager.createQuery(jpql, Long.class)
                .setParameter("userId", userId)
                .getSingleResult();
    }

    @Override
    public boolean existById(@NotNull final String id) {
        @NotNull final String jpql = "SELECT COUNT(m) = 1 FROM Session m WHERE m.id = :id";
        return entityManager.createQuery(jpql, Boolean.class)
                .setParameter("id", id)
                .getSingleResult();
    }

    @Override
    public boolean existById(@NotNull final String userId, @NotNull final String id) {
        @NotNull final String jpql = "SELECT COUNT(m) = 1 FROM Session m WHERE m.id = :id AND m.user.id  = :userId";
        return entityManager.createQuery(jpql, Boolean.class)
                .setParameter("id", id)
                .setParameter("userId", userId)
                .getSingleResult();
    }

}
