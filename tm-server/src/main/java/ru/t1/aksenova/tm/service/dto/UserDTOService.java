package ru.t1.aksenova.tm.service.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.aksenova.tm.api.repository.dto.IUserDTORepository;
import ru.t1.aksenova.tm.api.service.IConnectionService;
import ru.t1.aksenova.tm.api.service.IPropertyService;
import ru.t1.aksenova.tm.api.service.dto.IProjectDTOService;
import ru.t1.aksenova.tm.api.service.dto.ISessionDTOService;
import ru.t1.aksenova.tm.api.service.dto.ITaskDTOService;
import ru.t1.aksenova.tm.api.service.dto.IUserDTOService;
import ru.t1.aksenova.tm.dto.model.UserDTO;
import ru.t1.aksenova.tm.enumerated.Role;
import ru.t1.aksenova.tm.exception.entity.TaskNotFoundException;
import ru.t1.aksenova.tm.exception.entity.UserNotFoundException;
import ru.t1.aksenova.tm.exception.field.EmailEmptyException;
import ru.t1.aksenova.tm.exception.field.IdEmptyException;
import ru.t1.aksenova.tm.exception.field.LoginEmptyException;
import ru.t1.aksenova.tm.exception.field.PasswordEmptyException;
import ru.t1.aksenova.tm.exception.user.ExistEmailException;
import ru.t1.aksenova.tm.exception.user.ExistLoginException;
import ru.t1.aksenova.tm.exception.user.RoleEmptyException;
import ru.t1.aksenova.tm.repository.dto.UserDTORepository;
import ru.t1.aksenova.tm.util.HashUtil;

import javax.persistence.EntityManager;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

public class UserDTOService extends AbstractDTOService<UserDTO, IUserDTORepository> implements IUserDTOService {

    @NotNull
    private final IPropertyService propertyService;

    @NotNull
    private final IProjectDTOService projectService;

    @NotNull
    private final ITaskDTOService taskService;

    @NotNull
    private final ISessionDTOService sessionService;

    public UserDTOService(
            @NotNull final IPropertyService propertyService,
            @NotNull final IConnectionService connectionService,
            @NotNull final IProjectDTOService projectService,
            @NotNull final ITaskDTOService taskService,
            @NotNull final ISessionDTOService sessionService
    ) {
        super(connectionService);
        this.propertyService = propertyService;
        this.projectService = projectService;
        this.taskService = taskService;
        this.sessionService = sessionService;
    }


    @NotNull
    @Override
    public IUserDTORepository getRepository(@NotNull final EntityManager entityManager) {
        return new UserDTORepository(entityManager);
    }

    @NotNull
    @Override
    public UserDTO create(
            @Nullable final String login,
            @Nullable final String password
    ) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        if (isLoginExist(login)) throw new ExistLoginException(login);
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        @NotNull UserDTO user = new UserDTO();
        user.setLogin(login);
        user.setPasswordHash(HashUtil.salt(propertyService, password));
        add(user);
        return user;
    }

    @NotNull
    @Override
    public UserDTO create(
            @Nullable final String login,
            @Nullable final String password,
            @Nullable final String email
    ) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        if (isLoginExist(login)) throw new ExistLoginException(login);
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        if (isEmailExist(email)) throw new ExistEmailException(email);
        @NotNull UserDTO user = new UserDTO();
        user.setLogin(login);
        user.setPasswordHash(HashUtil.salt(propertyService, password));
        user.setEmail(email);
        add(user);
        return user;
    }

    @NotNull
    @Override
    public UserDTO create(
            @Nullable final String login,
            @Nullable final String password,
            @Nullable final Role role
    ) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        if (isLoginExist(login)) throw new ExistLoginException(login);
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        if (role == null) throw new RoleEmptyException();
        @NotNull UserDTO user = new UserDTO();
        user.setLogin(login);
        user.setPasswordHash(HashUtil.salt(propertyService, password));
        user.setRole(role);
        add(user);
        return user;
    }

    @NotNull
    @Override
    public Collection<UserDTO> set(@NotNull Collection<UserDTO> users) {
        if (users.isEmpty()) return Collections.emptyList();
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final IUserDTORepository repository = getRepository(entityManager);
            entityManager.getTransaction().begin();
            users.forEach(repository::add);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
        return users;
    }

    @NotNull
    @Override
    public List<UserDTO> findAll() {
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final IUserDTORepository repository = getRepository(entityManager);
            return repository.findAll();
        } finally {
            entityManager.close();
        }
    }

    @Nullable
    @Override
    public UserDTO findOneById(@Nullable final String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final IUserDTORepository repository = getRepository(entityManager);
            @Nullable final UserDTO user = repository.findOneById(id);
            if (user == null) throw new TaskNotFoundException();
            return user;
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    @Override
    public UserDTO findByLogin(@Nullable final String login) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final IUserDTORepository repository = getRepository(entityManager);
            @Nullable final UserDTO user = repository.findByLogin(login);
            if (user == null) throw new UserNotFoundException();
            return user;
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    @Override
    public UserDTO findByEmail(@Nullable final String email) {
        if (email == null || email.isEmpty()) throw new EmailEmptyException();
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final IUserDTORepository repository = getRepository(entityManager);
            @Nullable final UserDTO user = repository.findByEmail(email);
            if (user == null) throw new UserNotFoundException();
            return user;
        } finally {
            entityManager.close();
        }
    }

    @Nullable
    @Override
    public UserDTO removeOneById(@Nullable final String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final IUserDTORepository repository = getRepository(entityManager);
            @Nullable final UserDTO user = repository.findOneById(id);
            if (user != null) removeOne(user);
            return user;
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void clear() {
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final IUserDTORepository repository = getRepository(entityManager);
            entityManager.getTransaction().begin();
            repository.clear();
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Nullable
    @Override
    public UserDTO removeOne(@Nullable final UserDTO user) {
        if (user == null) throw new TaskNotFoundException();
        taskService.removeAll(user.getId());
        projectService.removeAll(user.getId());
        sessionService.removeAll(user.getId());
        remove(user);
        return user;
    }

    @Nullable
    @Override
    public UserDTO removeOneByLogin(@Nullable final String login) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        @NotNull final UserDTO user = findByLogin(login);
        return removeOne(user);
    }

    @Nullable
    @Override
    public UserDTO removeOneByEmail(@Nullable final String email) {
        if (email == null || email.isEmpty()) throw new EmailEmptyException();
        @Nullable final UserDTO user = findByEmail(email);
        return removeOne(user);
    }

    @NotNull
    @Override
    public UserDTO setPassword(
            @Nullable final String id,
            @Nullable final String password
    ) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        @Nullable final UserDTO user = findOneById(id);
        if (user == null) throw new UserNotFoundException();
        user.setPasswordHash(HashUtil.salt(propertyService, password));
        update(user);
        return user;
    }

    @NotNull
    @Override
    public UserDTO updateUser(
            @Nullable final String id,
            @Nullable final String firstName,
            @Nullable final String lastName,
            @Nullable final String middleName
    ) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @Nullable final UserDTO user = findOneById(id);
        if (user == null) throw new UserNotFoundException();
        user.setFirstName(firstName);
        user.setLastName(lastName);
        user.setMiddleName(middleName);
        update(user);
        return user;
    }

    @Override
    public boolean isLoginExist(@Nullable final String login) {
        if (login == null || login.isEmpty()) return false;
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final IUserDTORepository repository = getRepository(entityManager);
            UserDTO user = repository.findByLogin(login);
            return (user != null);
        } finally {
            entityManager.close();
        }
    }

    @Override
    public boolean isEmailExist(@Nullable final String email) {
        if (email == null || email.isEmpty()) return false;
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final IUserDTORepository repository = getRepository(entityManager);
            return (repository.findByEmail(email) != null);
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void lockUserByLogin(@Nullable final String login) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        @NotNull final UserDTO user = findByLogin(login);
        user.setLocked(true);
        update(user);
    }

    @Override
    public void unlockUserByLogin(@Nullable final String login) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        @NotNull final UserDTO user = findByLogin(login);
        user.setLocked(false);
        update(user);
    }

}
