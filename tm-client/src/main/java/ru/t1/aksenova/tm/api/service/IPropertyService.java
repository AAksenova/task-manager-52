package ru.t1.aksenova.tm.api.service;

import org.jetbrains.annotations.NotNull;
import ru.t1.aksenova.tm.api.endpoint.IConnectionProvider;

public interface IPropertyService extends IConnectionProvider {

    @NotNull
    String getApplicationVersion();

    @NotNull
    String getApplicationConfig();

    @NotNull
    String getAuthorName();

    @NotNull
    String getAuthorEmail();

    @NotNull
    String getGitBranch();

    @NotNull
    String getGitCommitId();

    @NotNull
    String getGitCommitTime();

    @NotNull
    String getGitCommitMessage();

    @NotNull
    String getGitCommitterName();

    @NotNull
    String getGitCommitterEmail();

}
